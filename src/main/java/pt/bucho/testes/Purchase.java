package pt.bucho.testes;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="purchase", schema="tables")
public class Purchase {

	@Id
	@GeneratedValue
	private int id;
	private String description;
	@ManyToOne(targetEntity=Person.class)
	private Person purchaser;
	private float value;
	
	public Purchase() {
		this.description = "";
		this.purchaser = null;
		this.value = 0;
	}

	public Purchase(String description, Person purchaser, float value) {
		this.description = description;
		this.purchaser = purchaser;
		this.value = value;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Person getPurchaser() {
		return purchaser;
	}

	public void setPurchaser(Person purchaser) {
		this.purchaser = purchaser;
	}

	public float getValue() {
		return value;
	}

	public void setValue(float value) {
		this.value = value;
	}
	
}
