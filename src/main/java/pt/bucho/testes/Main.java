package pt.bucho.testes;

import java.util.Date;
import java.util.List;

import org.hibernate.service.spi.ServiceException;

public class Main {

	private static DAO dao;

	public static void main(String[] args) {

		try {
			dao = new DAO();
		} catch (ServiceException e) {
			System.out.println(e.getMessage());
			Throwable cause1 = e.getCause();
			System.out.println(cause1.getMessage());
			Throwable cause2 = cause1.getCause();
			System.out.println(cause2.getMessage());

			System.exit(-1);
		}

		Person person = new Person("Pedro", new Date(), "pmtb");

		dao.save(person);
		
		List<Person> persons = dao.persons();

		for (Person person2 : persons) {
			System.out.println(person2);
		}

		Purchase p1 = new Purchase("Compra1", person, 2.25f);
		
		dao.save(p1);
		
		dao.clearSession();
		
	}

}
