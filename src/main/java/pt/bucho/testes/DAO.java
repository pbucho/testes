package pt.bucho.testes;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

public class DAO {
	
	private SessionFactory sessionFactory;
	
	public DAO() {
		Configuration configuration = new Configuration().configure()
				.addAnnotatedClass(Person.class)
				.addAnnotatedClass(Purchase.class);
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties());
		sessionFactory = configuration.buildSessionFactory(builder.build());
	}
	
	private Session getSession() {
		Session session = sessionFactory.openSession();
		session.setFlushMode(FlushMode.COMMIT);
		session.beginTransaction();
		return session;
	}
	
	public Person personById(int id) {
		Criteria criteria = getSession().createCriteria(Person.class)
				.add(Restrictions.eq("id", id));
		return (Person) criteria.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<Person> persons() {
		Criteria criteria = getSession().createCriteria(Person.class);
		return criteria.list();
	}
	
	public Person personByIdentification(String identification) {
		Criteria criteria = getSession().createCriteria(Person.class)
				.createAlias("identification", "identification")
				.add(Restrictions.eq("identification", identification));
		return (Person) criteria.uniqueResult();
	}
	
	public void save(Object object) {
		Session session = getSession();
		session.save(object);
	}
	
	public void persistObjects(Object ... objects){
		for(Object object : objects){
			save(object);
		}
	}
	
	public void clearSession(){
		sessionFactory.close();
	}
	
}
