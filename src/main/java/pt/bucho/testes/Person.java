package pt.bucho.testes;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="Person", schema="tables")
public class Person {

	@Id
	@GeneratedValue
	private int id;
	private String name;
	private Date birthday;
	private String identification;

	public Person() {
		this.name = "";
		this.birthday = new Date();
		this.identification = "";
	}
	
	public Person(String name, Date birthday, String identification) {
		this.name = name;
		this.birthday = birthday;
		this.identification = identification;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getIdentification() {
		return identification;
	}
	public void setIdentification(String identification) {
		this.identification = identification;
	}
	
	@Override
	public String toString() {
		return identification + ": " + name + "(" + id + ")";
	}
	
}
