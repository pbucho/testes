package pt.bucho.testes;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import pt.bucho.testes.DAO;
import pt.bucho.testes.Person;

public class PersonPersistenceTest {

	private DAO dao;
	private Person person;
	private Date birthday;

	@Before
	public void setUp() {
		dao = new DAO();

		birthday = new Date();

		person = new Person();
		person.setName("Pedro");
		person.setIdentification("PEDRO");
		person.setBirthday(birthday);

	}

	@Test
	public void personPersistence() {

		dao.save(person);

		dao.clearSession();

		Person retrievedPerson = null;
		try {
			retrievedPerson = dao.personByIdentification("PEDRO");
		} catch (Exception e) {
			System.out.println("CAUGHT EXPECTION: " + e.getMessage());
		}

		assertEquals("Pedro", retrievedPerson.getName());
		assertEquals("PEDRO", retrievedPerson.getId());
		assertEquals(birthday, retrievedPerson.getBirthday());

	}

}
